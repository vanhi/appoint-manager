package com.vanhi.appointmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppointManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppointManagerApplication.class, args);
    }

}
